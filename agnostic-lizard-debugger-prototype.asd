(asdf:defsystem
  :agnostic-lizard-debugger-prototype

  ; It is still a bit of a proof of concept — if there is an established
  ; opensource project that would benefit from shipping 
  ; agnostic-lizard-debugger-prototype, adding more license options
  ; will be considered
  :licence "GPLv3+"

  :description
  "A portable debugger prototype that injects debugger hooks into code"

  :author "Michael Raskin <38a938c2@rambler.ru>"

  :depends-on (:bordeaux-threads :agnostic-lizard)

  :components
  ((:static-file "README")
   (:static-file "AUTHORS")
   (:static-file "COPYING")
   (:static-file "gpl-3.0.txt")
   (:file "debugger-prototype")))
