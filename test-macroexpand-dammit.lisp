(require :macroexpand-dammit)

(defmacro f () 1)

(defmacro macroexpand-dammit-here (form &environment env)
  `',(macroexpand-dammit:macroexpand-dammit form env))

(macrolet ((f () 2)) (macroexpand-dammit-here (macrolet () (f))))

(macrolet ((f () 2)) (macrolet () (f)))
